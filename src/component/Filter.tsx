import {Button, Card, Col, InputNumber, Row} from "antd";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { SET_MAX_PRICE, SET_MIN_PRICE, SET_MIN_QTY } from "../action/bookAction";


const Filter = () => {
    const [minValue, setMinValue] = useState(10);
    const [maxValue, setMaxValue] = useState(100);
    const [minQty, setMinQty] = useState(1);
    

    const handleMinPrice = (val: any) => {
        setMinValue(val);
    };
    const handleMaxPrice = (val: any) => {
        setMaxValue(val);
    };
    const handleMinQty = (val: any) => {
        setMinQty(val);
    };

    const dispatch = useDispatch();
    function handleFilter() 
    {
        dispatch(SET_MAX_PRICE(maxValue));
        dispatch(SET_MIN_PRICE(minValue));
        dispatch(SET_MIN_QTY(minQty));
    }

    return <Card>
        <Row>
           <Col span={6}>
               <InputNumber addonBefore={'Min Price'} value={minValue} onChange={(e: any) => handleMinPrice(e)}/>
           </Col>
            <Col span={6}>
                <InputNumber addonBefore={'Max Price'} value={maxValue} onChange={(e: any) => handleMaxPrice(e)}/>
            </Col>
            <Col span={6}>
                <InputNumber addonBefore={'Min Qty'} value={minQty} onChange={(e: any) => handleMinQty(e)} />
            </Col>
            <Col span={6}>
                <Button type={"primary"} onClick={handleFilter}>Filter</Button>
            </Col>
        </Row>
    </Card>
}

export default Filter;