import {Card, Avatar, InputNumber, Row, Col} from "antd";

import { EditOutlined, EllipsisOutlined, SettingOutlined, PlusOutlined, MinusOutlined } from '@ant-design/icons';
import {FC, useState} from "react";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";

import { SET_TOTAL_BOOKS, SET_TOTAL_BOOKS_PRICE } from "../action/bookAction";

const { Meta } = Card;

export interface IBook{
    id: number;
    title: string;
    description: string;
    totalQuantity: number;
    pricePerQty: number
}
interface IProps {
    book?: IBook
}

const Book: FC<IProps> = ({book={}}) => {
    const {
        id=1212,
        title = "Book Name",
        description = "This is really nice book",
        totalQuantity = 10,
        pricePerQty = 135
    } = book;

    const [qty, setQty] = useState<number>(0);
    const [totalPrice, setTotalPrice] = useState<number>(0);
    const [disablePlus, setDisablePlus] = useState(false);
    const [disableMinus, setDisableMinus] = useState(false);
    const [currentQty, setCurrentQty] = useState(0);

    const totalBooks = useSelector((state: RootStateOrAny) => state.totalBooks);
    const totalBooksPrice = useSelector((state: RootStateOrAny) => state.totalBooksPrice);
    const dispatch = useDispatch();

    function incrementQty() 
    {
        if(qty < totalQuantity)
        {
            setQty(qty+1);
            setCurrentQty(qty+1);
            setTotalPrice((qty+1) * pricePerQty);
            dispatch(SET_TOTAL_BOOKS(totalBooks + 1));
            dispatch(SET_TOTAL_BOOKS_PRICE(totalBooksPrice +  pricePerQty));
        }
        else
        {
            setDisablePlus(true);
        }
    }
    
    function deccrementQty() 
    {
        if(qty>0)
        {
            setQty(qty-1);
            setCurrentQty(qty-1);
            setTotalPrice((qty-1) * pricePerQty);
            dispatch(SET_TOTAL_BOOKS(totalBooks - 1));
            dispatch(SET_TOTAL_BOOKS_PRICE(totalBooksPrice - pricePerQty));
        }
        else
        {
            setDisableMinus(true);
        }
    }

    return <>
        <Card
            style={{ width: '80%', margin: '20px' }}
            actions={[
                    <PlusOutlined disabled={disablePlus} onClick={incrementQty}/>,
                    <InputNumber disabled={false} value={currentQty} readOnly/>,
                    <MinusOutlined disabled={disableMinus} onClick={deccrementQty}/>
            ]}
        >
            <Row>
                <Col span={4}>ID: {id}</Col>
                <Col span={8}>Name: {title}</Col>
                <Col span={12}>Description: {description}</Col>
            </Row>
            <Row>
                <Col span={12}>Total Quantity: {totalQuantity} Pc</Col>
                <Col span={12}>Price per Qty: INR {pricePerQty}</Col>
            </Row>

            <Row>
                <Col span={12}>Order Quantity: {qty} Pc</Col>
                <Col span={12}>Total Price: INR {totalPrice}</Col>
            </Row>
        </Card>
    </>
}
export default Book;