import {Card, Col, Row, Statistic} from "antd";
import { RootStateOrAny, useSelector } from "react-redux";

const Cart = () => {
    const totalBooks = useSelector((state: RootStateOrAny) => state.totalBooks);
    const totalBooksPrice = useSelector((state: RootStateOrAny) => state.totalBooksPrice);



    return (
        <Card>
            <Row gutter={16}>
                <Col span={8}>
                    <Statistic title="Total Books to Order" value={`${totalBooks} Pc`} />
                </Col>
                <Col span={8}>
                    <Statistic title="Total Discount" value={`${(totalBooks>10 || totalBooksPrice>1000)? ((totalBooksPrice*0.1).toFixed(2)) : 0} INR`}  />
                </Col>
                <Col span={8}>
                    <Statistic title="Total Amount" value={`${(totalBooks>10 || totalBooksPrice>1000)? ((totalBooksPrice*0.9).toFixed(2)) : totalBooksPrice} INR`} />
                </Col>
            </Row>
        </Card>
    )
}
export default Cart;