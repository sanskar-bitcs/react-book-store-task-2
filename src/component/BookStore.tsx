import {useState, useEffect} from "react";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import {Button, Spin} from "antd";
import { click } from "@testing-library/user-event/dist/click";
import {IBook} from './Book'
import Book from "./Book";
import Cart from "./Cart";
import Filter from "./Filter";
import BookService from "../service/book-service";
import { SET_PAGE_COUNT } from "../action/bookAction";

const BookStore = () => {

    const [isSpinning, setSpinning] = useState(true);
    const [books, setBooks] = useState<Array<IBook>>([]);
    const [disable, setDisable] = useState(false);
    const minPrice = useSelector((state: RootStateOrAny) => state.minPrice);
    const maxPrice = useSelector((state: RootStateOrAny) => state.maxPrice);
    const minQty = useSelector((state: RootStateOrAny) => state.minQty);
    const page = useSelector((state: RootStateOrAny) => state.page);
    const dispatch = useDispatch();
    const getBooks = async () => {
        const booksData = await BookService.fetchBooks({minPrice, maxPrice, minQty, page});
        return booksData;
    }

    function handleLoad() {
        setSpinning(true);
        let booksData = getBooks();
        
        dispatch(SET_PAGE_COUNT(page+1));
        booksData.then((res) => {
            if(res.length===0){
                setDisable(true);
                dispatch(SET_PAGE_COUNT(1));
            }
            else{
            setBooks([...books, ...res]);
            }
            setSpinning(false);
        })
        .catch((err) => 
        {
            setSpinning(false);
            alert(err);
        });
    }

    
    useEffect(() => {
       dispatch(SET_PAGE_COUNT(2));
        setSpinning(true);
        setDisable(false);
        let booksData = getBooks();

        booksData.then((res) => {
            setBooks([...res]);
            setSpinning(false);
        })
        .catch((err) => 
        {
            setSpinning(false);
            alert(err);
        });
        
    }, [minPrice, maxPrice, minQty]);

    return <Spin spinning={isSpinning}>
        <Filter/>
        {
            books.map((book) => {
                return (<Book book={book} key={book.id}/>)
            })
        }
        <Button type={'primary'} disabled={disable} onClick={handleLoad} > Load More </Button>
        <Cart/>
    </Spin>
}

export default BookStore;