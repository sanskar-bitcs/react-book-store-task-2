export const SET_MIN_PRICE = (payload: Number) => {
    return {
        type: "SET_MIN_PRICE",
        payload: payload
    };
}

export const SET_MAX_PRICE = (payload: Number) => {
    return {
        type: "SET_MAX_PRICE",
        payload: payload
    };
}

export const SET_MIN_QTY = (payload: Number) => {
    return {
        type: "SET_MIN_QTY",
        payload: payload
    };
}

export const SET_TOTAL_BOOKS = (payload: Number) => {
    return {
        type: "SET_TOTAL_BOOKS",
        payload: payload
    };
}

export const SET_TOTAL_BOOKS_PRICE = (payload: Number) => {
    return {
        type: "SET_TOTAL_BOOKS_PRICE",
        payload: payload
    };
}

export const SET_PAGE_COUNT = (payload: Number) => {
    return {
        type: "SET_PAGE_COUNT",
        payload: payload
    };
}

