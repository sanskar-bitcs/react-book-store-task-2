const initState = {
    minPrice: 10, 
    maxPrice: 100, 
    minQty: 1,
    totalBooks: 0,
    totalBooksPrice: 0,
    page : 1
};

export const bookReducer = (initialState = initState, action: any) => {
    switch(action.type)
    {
        case "SET_MIN_PRICE":
            return {...initialState, minPrice: action.payload}
        case "SET_MAX_PRICE":
            return {...initialState, maxPrice: action.payload}
        case "SET_MIN_QTY":
            return {...initialState, minQty: action.payload}
        case "SET_TOTAL_BOOKS":
            return {...initialState, totalBooks: action.payload}
        case "SET_TOTAL_BOOKS_PRICE":
            return {...initialState, totalBooksPrice: action.payload}
        case "SET_PAGE_COUNT":
            return {...initialState, page : action.payload}
        default: 
            return initialState;
    }
}